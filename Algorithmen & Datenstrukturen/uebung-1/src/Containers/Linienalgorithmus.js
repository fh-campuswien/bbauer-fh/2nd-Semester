import React, { Component } from 'react';

class Linienalgorithmus extends Component {

	state = {
		coordinates: {
			p1x: 3,
			p1y: 2,
			p2x: 20,
			p2y: 14
		}
	};
	line = [];

	inputChangeHandler = (event) => {
		if (!event.target.value || isNaN(event.target.value)) {
			return;
		}

		let new_coordinates = {...this.state.coordinates};
		new_coordinates[event.target.name] = parseInt(event.target.value, 0);
		this.setState({ coordinates: new_coordinates });
	}

	buildLineCoordinates = (p1x, p1y, p2x, p2y) => {
		const distance = Math.pow(p1x-p2x, 2)+Math.pow(p1y-p2y, 2);
		if (distance < 4) {
			return;
		}

		const mx = parseInt(Math.abs(p1x+p2x)/2, 0);
		const my = parseInt(Math.abs(p1y+p2y)/2, 0);
		this.line.push([mx, my]);

		this.buildLineCoordinates(p1x, p1y, mx, my);
		this.buildLineCoordinates(mx, my, p2x, p2y);
	}

	buildTableOutput() {
		this.line = [[
			(this.state.coordinates.p1x >= this.state.coordinates.p2x) ? this.state.coordinates.p2x : this.state.coordinates.p1x,
			(this.state.coordinates.p1y >= this.state.coordinates.p2y) ? this.state.coordinates.p2y : this.state.coordinates.p1y,
		], [
			(this.state.coordinates.p1x >= this.state.coordinates.p2x) ? this.state.coordinates.p1x : this.state.coordinates.p2x,
			(this.state.coordinates.p1y >= this.state.coordinates.p2y) ? this.state.coordinates.p1y : this.state.coordinates.p2y,
		]];
		this.buildLineCoordinates(
			(this.state.coordinates.p1x >= this.state.coordinates.p2x) ? this.state.coordinates.p2x : this.state.coordinates.p1x,
			(this.state.coordinates.p1y >= this.state.coordinates.p2y) ? this.state.coordinates.p2y : this.state.coordinates.p1y,
			(this.state.coordinates.p1x >= this.state.coordinates.p2x) ? this.state.coordinates.p1x : this.state.coordinates.p2x,
			(this.state.coordinates.p1y >= this.state.coordinates.p2y) ? this.state.coordinates.p1y : this.state.coordinates.p2y,
		);

		let table_content;
		const xmax = Math.max(this.state.coordinates.p1x, this.state.coordinates.p2x) + 1;
		const ymax = Math.max(this.state.coordinates.p1y, this.state.coordinates.p2y) + 1;

		var x = Array.apply(0, Array(xmax));
		var y = Array.apply(0, Array(ymax));

		table_content = y.map((elem, ypos) => {
			const xvalues = x.map((el, xpos) => {
				let is_line = false;
				this.line.forEach(function(pos) {
					if (pos[0] === (xpos+1) && pos[1] === (ypos+1)) {
						is_line = true;
					}
				});

				if (is_line) {
					return <td key={(xpos+1)+(ypos+1)} className={'blue'}></td>;
				} else {
					return <td key={(xpos+1)+(ypos+1)}></td>;
				}
			});
			return <tr key={(ypos-1)}>{xvalues}</tr>;
		});
		return table_content;
	}

	render() {
		return (
			<div className={'line-algo'}>
				<h1>Bsp 2: Linienalgorithmus</h1>
				<p>Bitte geben Sie 2 Punkte ein</p>
				<div>
					{ Object.keys(this.state.coordinates).map((key) => {
						return <input
							key={ key }
							name={ key }
							placeholder={ key }
							type="number"
							onChange={ this.inputChangeHandler }
							value={ this.state.coordinates[key] } />;
					}) }
				</div>


				<table>
					<tbody>
						{ this.buildTableOutput() }
					</tbody>
				</table>
			</div>
		);
	}

}

export default Linienalgorithmus;
