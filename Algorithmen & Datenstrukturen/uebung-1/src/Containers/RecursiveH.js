import React, { Component } from 'react';

class RecursiveH extends Component {

	state = {
		output: null
	};
	output = [];

	recursive = (n) => {
		if (n === 1) {
			this.output.push("1");
			return 1;
		} else if (n < 1 || isNaN(n)) {
			this.output.push("0");
			return 0;
		} else {
			let calc = this.recursive(n-1) + 1/n;
			this.output.push("1/"+n);
			return calc;
		}
	}

	inputChangeHandler = (event) => {
		if (event.target.value) {
			this.setState({ input: event.target.value });
		}
	}

	componentDidMount() {
		this.nameInput.focus();
	}

	render() {
		this.output = [];
		let content = <p>Geben Sie zuerst eine Zahl ein</p>;
		if (!isNaN(this.state.input)) {
			const result = this.recursive(parseInt(this.state.input, 0));
			content = <p><b>H(n)</b> = {this.output.join(' + ')} = <b>{result.toFixed(2)}</b></p>;
		}

		return (
			<div className={['recursion']}>
				<h1>Bsp 1: Rekursion</h1>
				<p>Bitte geben Sie eine Zahl ein</p>
				<input
					ref={(input) => { this.nameInput = input; }}
					type="number"
					onChange={ this.inputChangeHandler } />
				{ content }
			</div>
		);
	}

}

export default RecursiveH;
