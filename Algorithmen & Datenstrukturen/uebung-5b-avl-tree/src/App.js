import React, { Component } from 'react';
import ReactTooltip from 'react-tooltip';
import Tree from 'react-d3-tree';
import faker from 'faker';
import { NotificationContainer, NotificationManager } from 'react-notifications';

import './App.scss';
import 'react-notifications/lib/notifications.css';

import AVLTree from './avl-tree.js';
import TreeNode from './tree-node.js';

class App extends Component {

	avlTree = null;
	state = {
		data: null,
		nodeSvgShape: {
			shape: 'circle',
			shapeProps: {
				r: 12,
			}
		},
		// pathFunc: (d, orientation) => orientation && `M${d.source.y},${d.source.x}V${d.target.x}H${d.target.y}`,
		circleRadius: undefined,
		orientation: 'vertical',
		collapsible: true,
		initialDepth: undefined,
		depthFactor: undefined,
		zoomable: true,
		zoom: 1,
		scaleExtent: { min: 0.1, max: 1 },
		separation: { siblings: 1, nonSiblings: 2 },
		nodeSize: { x: 140, y: 140 },
		transitionDuration: 500,
		textLayout: {
			textAnchor: "start",
			x: 20,
			y: 0,
			transform: undefined
		},
		styles: {
			nodes: {
				node: {
					circle: {
						fill: '#c70039',
					}
				},
				leafNode: {
					circle: {
						fill: '#ffc300',
					}
				}
			}
		}
	};

	componentDidMount() {
		this.avlTree = new AVLTree();

		let node = new TreeNode(10, "hello");
		this.avlTree.insert(node);
		node = new TreeNode(5, "hello");
		this.avlTree.insert(node);
		node = new TreeNode(12, "hello");
		this.avlTree.insert(node);
		node = new TreeNode(7, "hello");
		this.avlTree.insert(node);
		node = new TreeNode(9, "hello");
		this.avlTree.insert(node);
		node = new TreeNode(4, "hello");
		this.avlTree.insert(node);

		if (this.state.data === null) {
			this.setState({ data: this.avlTree.getReactD3TreeObject() });
		}
	}

	onInsert = (event) => {
		let personId = document.getElementById("idInput");
		let personName = document.getElementById("nameInput");

		if (personId.value !== "" && personName.value !== "") {
			let node = new TreeNode(parseInt(personId.value, 0), personName.value);

			if (this.avlTree.insert(node)) {
				this.setState({ data: this.avlTree.getReactD3TreeObject() });
			} else {
				NotificationManager.warning('Could not insert new data set!');
			}

			// Clear input fields
			personId.value = '';
			personName.value = '';
		} else {
			NotificationManager.warning('Insufficient data provided. Id and name necessary.');
		}
	}

	onSearch = (event) => {
		let searchIdInput = document.getElementById("searchIdInput");

		if (searchIdInput.value) {
			const node = this.avlTree.lookup(parseInt(searchIdInput.value, 0));
			if (node) {
				NotificationManager.success('Person "'+node.data+'" found for key '+node.key+'.');
			} else {
				NotificationManager.info('Node with key '+searchIdInput.value+' not found.');
			}
			searchIdInput.value = '';
		} else {
			NotificationManager.warning('No key given which may be used to search for a person.');
		}
	}

	onInsertRandom = (event) => {
		let insertAmount = 5;

		// If there is no root node, insert a one with id between 4750 and 5250
		if (this.avlTree.root === null) {
			let node = new TreeNode(Math.floor(Math.random() * 501) + 4750, faker.fake("{{name.firstName}} {{name.lastName}}"));
			this.avlTree.insert(node);
			insertAmount--;
		}

		for (var i = 0; i < insertAmount; i++) {
			let node = new TreeNode(Math.floor(Math.random() * 9999) + 1, faker.fake("{{name.firstName}} {{name.lastName}}"));
			this.avlTree.insert(node);
		}
		this.setState({ data: this.avlTree.getReactD3TreeObject() });
	}

	onClearAll = () => {
		this.avlTree.root = null;
		document.getElementById("idInput").value = '';
		document.getElementById("nameInput").value = '';
		document.getElementById("searchIdInput").value = '';
		this.setState({ data: this.avlTree.getReactD3TreeObject() });
	}

	handleKeyPress = (event) => {
		if (event.key === 'Enter') {
			if (event.target.id === "idInput" || event.target.id === "nameInput") {
				this.onInsert(event);
			} else if (event.target.id === "searchIdInput") {
				this.onSearch(event);
			} else {
				console.log('Invalid element "'+event.target.id+'" for key press event!');
			}
		}
	}

	render() {
		const browserWidth = Math.max(
			document.body.scrollWidth,
			document.documentElement.scrollWidth,
			document.body.offsetWidth,
			document.documentElement.offsetWidth,
			document.documentElement.clientWidth
		);
		const treeTranslateX =  browserWidth/2;
		const treeTranslateY = (browserWidth > 1600) ? 400 : 500;


		let renderedAVLTree = null;
		if (this.state.data) {
			renderedAVLTree = (
				<Tree
					data={this.state.data}
					nodeSvgShape={this.state.nodeSvgShape}
					circleRadius={this.state.circleRadius}
					onClick={this.state.onClick}
					orientation={this.state.orientation}
					translate={{ x: treeTranslateX, y: treeTranslateY }}
					pathFunc={this.state.pathFunc}
					collapsible={this.state.collapsible}
					initialDepth={this.state.initialDepth}
					zoomable={this.state.zoomable}
					zoom={this.state.zoom}
					scaleExtent={this.state.scaleExtent}
					nodeSize={this.state.nodeSize}
					separation={this.state.separation}
					transitionDuration={this.state.transitionDuration}
					depthFactor={this.state.depthFactor}
					textLayout={this.state.textLayout}
					styles={this.state.styles} />
			);
		}

		return (
			<div className="App">
				<div className="insertPane">
					<h1>AVL Tree (Depth: { this.avlTree !== null ? this.avlTree.treeDepth() : 0 })</h1>

					<div className="inputWrapper">
						<input type="number" min="1" max="9999" id="idInput" placeholder="Person Nr." data-tip="Number between 1 and 9999" onKeyPress={this.handleKeyPress} />
						<input type="text" id="nameInput" placeholder="Name" onKeyPress={this.handleKeyPress} />
						<input type="submit" value="Insert" onClick={this.onInsert} />
					</div>

					<div className="inputWrapper">
						<input type="number" min="1" max="9999" id="searchIdInput" placeholder="Person Nr." data-tip="Number between 1 and 9999" onKeyPress={this.handleKeyPress} />
						<input type="submit" value="Search by Person Nr." onClick={this.onSearch} />
					</div>

					<div className="inputWrapper">
						<input type="submit" value="Add 5 random entries" onClick={this.onInsertRandom} />
						<input type="submit" value="Clear all data" onClick={this.onClearAll} />
					</div>
				</div>

				<div className="treeWrapper">
					{ renderedAVLTree }
				</div>

				{/* Init tooltips */}
				<ReactTooltip place="top" type="light" effect="solid" />
				<NotificationContainer />
			</div>
		);
	}
}

export default App;
