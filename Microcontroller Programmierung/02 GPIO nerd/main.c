// Header mit allen Adressen
#include "stm32f4xx.h"

int main(void) {
	
	int i;
	
	// Die LED soll leuchten.
	// DAfuer brauchen wir DREI Schritte
	// Schritt 1: Port GPIOA mittels RCC (Clock Enable Register) betakten
	// Schritt 2: GPIOA.5 als AUSGANG konfigurieren (Mode Register)
	// Schritt 3: dort einen EINSER rausschicken (Output Data Register)
	
	// AH81CLKEHA -> 0x4002 3800 + 30
	// ***1*** das Register hat die Adresse 0x4002 3830
	// und darin soll das Bit NR0 auf 1 gesetzt werden (Enable GPIO A)
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
	
	
	// ***2*** ModeRegister SO einstellen, dass der Pin A.5 ein AUSGANG wird...
	GPIOA->MODER |= GPIO_MODER_MODE5_0;
	// *((unsigned long*)0x40020000) |= (1<<10); // Bit 11 und 10 sind jetzt 01 ... OUTPUT
	
	
	// ***3*** OUTPUT DATA REGISTER muss einen 1er haben (ann Stelle 5, eh klar)
	GPIOA->ODR |= GPIO_ODR_OD5;
	// *((unsigned long *)0x40020014) |= (1<<5); // Pin5 auf 1 setzen
	
	
	// Endlosschleife, denn sonst wuerde der Kern
	// die Arbeit einstellen und das ist eigentlich
	// nicht gewuenscht...
	while(1) {
		GPIOA->ODR ^= GPIO_ODR_OD5;
		
		for(i=0; i<1000000; i++) {}
	}
	
}
